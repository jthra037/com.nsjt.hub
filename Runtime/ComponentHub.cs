﻿// ========================================================================================
// ComponentHub - A hub for finding Unity Components from anywhere
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System;
using System.Collections.Generic;
using System.Linq;

namespace NSJT.ComponentHub
{
	using UnityEngine;

	static public partial class Hub
	{
		private class ComponentRegistrar : IDisposable
		{
			private Dictionary<Type, List<Component>> components = new Dictionary<Type, List<Component>>();

			private Dictionary<Type, IReceiverList> registrationListeners =
				new Dictionary<Type, IReceiverList>();

			public void Dispose()
			{
				components.Clear();
				registrationListeners.Clear();
			}

			/// <summary>
			/// Async Getter for a component of a given type
			/// </summary>
			/// <typeparam name="T">Type of component</typeparam>
			/// <returns>The proper signal binding</returns>
			public int Get<T>(Action<T> receiver, Predicate<T> predicate) where T : Component
			{
				Type type = typeof(T);
				List<Component> boxedComponents;

				Receiver<T> receiverObject = new Receiver<T>()
				{
					Setter = receiver,
					Predicate = predicate
				};

				int hashCode = receiverObject.GetHashCode();

				// component already registered
				if (components.TryGetValue(type, out boxedComponents)
					&& tryGetComponent(boxedComponents, predicate, out T component))
				{
					receiver?.Invoke(component);
				}

				// listeners already exist for this component type
				if (registrationListeners.TryGetValue(type,
					out IReceiverList listeners))
				{
					listeners.UnBox<T>().Add(receiverObject);
				}
				else
				{
					registrationListeners.Add(type, new ReceiverList<T>(receiverObject));
				}

				return hashCode;
			}

			public void UnGet<T>(int hashCode) where T : Component
			{
				// component already registered
				if (registrationListeners.TryGetValue(typeof(T), out IReceiverList boxedListeners))
				{
					IReceiverList<T> listeners = boxedListeners.UnBox<T>();
					listeners.Remove(hashCode);

					// propagate changes back
					if (listeners.Count == 0)
					{
						registrationListeners.Remove(typeof(T));
					}
					else
					{
						registrationListeners[typeof(T)] = listeners;
					}
				}
			}

			internal void Register<T>(T component) where T : Component
			{
				if (components.TryGetValue(typeof(T), out List<Component> componentList))
				{
					componentList.Add(component);
				}
				else
				{
					components.Add(typeof(T), new List<Component>()
					{
						component
					});
				}

				invokeListeners(component);
			}

			internal void Unregister<T>(T component) where T : Component
			{
				if (components.TryGetValue(typeof(T), out List<Component> componentList))
				{
					componentList.Remove(component);
					if (componentList.Count == 0)
					{
						components.Remove(typeof(T));
					}
				}
			}

			// This method also used to remove listeners after firing them; not any more
			private void invokeListeners<T>(T value) where T : Component
			{
				Type type = typeof(T);

				if (registrationListeners.TryGetValue(type,
					out IReceiverList boxedListeners))
				{
					IReceiverList<T> listeners = boxedListeners.UnBox<T>();

					foreach (Receiver<T> listener in listeners.Where(receiver => receiver.Predicate(value)))
					{
						listener.Setter(value);
					}
				}
			}

			private bool tryGetComponent<T>(List<Component> boxedComponents, Predicate<T> predicate, out T value) where T : Component
			{
				// Should be either one or none
				// also, we should already have verified that T is the correct type
				value = boxedComponents.Where(comp => predicate(comp as T)).Cast<T>().FirstOrDefault();
				return value != default;
			}
		}

		private static readonly ComponentRegistrar registrar = new ComponentRegistrar();

		public static int Get<T>(Action<T> receiver) where T : Component
		{
			return registrar.Get(receiver, value => true);
		}

		public static int Get<T>(Action<T> receiver, Predicate<T> predicate) where T : Component
		{
			return registrar.Get(receiver, predicate);
		}

		public static void UnGet<T>(int hashCode) where T : Component
		{
			registrar.UnGet<T>(hashCode);
		}

		public static void Register<T>(T component) where T : Component
		{
			registrar.Register(component);
		}

		public static void UnRegister<T>(T component) where T : Component
		{
			registrar.Unregister(component);
		}

		private static bool truePredicate<T>(T _)
		{
			return true;
		}

		public static void Clear()
		{
			registrar.Dispose();
		}
	}
}
