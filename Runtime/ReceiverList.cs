﻿// ========================================================================================
// ComponentHub - A hub for finding Unity Components from anywhere
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NSJT.ComponentHub
{
	using UnityEngine;

	static public partial class Hub
	{
		private interface IReceiverList
		{
			IReceiverList<T> UnBox<T>() where T : Component;
		}

		private interface IReceiverList<T> : IReceiverList, IList<Receiver<T>> where T : Component
		{
			bool Remove(int hashCode);
		}

		// Concrete implementation let's us do compile the interface unboxing
		private class ReceiverList<T> : IReceiverList, IReceiverList<T> where T : Component
		{
			private List<Receiver<T>> list = new List<Receiver<T>>();

			// For convenience
			public ReceiverList(Receiver<T> receiver)
			{
				list.Add(receiver);
			}

			public ReceiverList(IEnumerable<Receiver<T>> receivers)
			{
				list.AddRange(receivers);
			}

			#region IReceiverList<T> Implementation
			public Receiver<T> this[int index]
			{
				get => list[index];
				set => list[index] = value;
			}

			public int Count => list.Count();

			public bool IsReadOnly => false;

			public void Add(Receiver<T> item)
			{
				list.Add(item);
			}

			public void Clear()
			{
				list.Clear();
			}

			public bool Contains(Receiver<T> item)
			{
				return list.Contains(item);
			}

			public void CopyTo(Receiver<T>[] array, int arrayIndex)
			{
				list.CopyTo(array, arrayIndex);
			}

			public IEnumerator<Receiver<T>> GetEnumerator()
			{
				return list.GetEnumerator();
			}

			public int IndexOf(Receiver<T> item)
			{
				return list.IndexOf(item);
			}

			public void Insert(int index, Receiver<T> item)
			{
				list.Insert(index, item);
			}

			public bool Remove(Receiver<T> item)
			{
				return list.Remove(item);
			}

			public bool Remove(int hashCode)
			{
				int count = list.Count;
				list = list.Where(receiver => receiver.GetHashCode() != hashCode).ToList();
				return list.Count < count;
			}

			public void RemoveAt(int index)
			{
				list.RemoveAt(index);
			}
			#endregion

			#region IReceiverList Implementation
			public IReceiverList<V> UnBox<V>() where V : Component
			{
				if (typeof(V) == typeof(T))
				{
					return (IReceiverList<V>)this;
				}
				else
				{
					throw new NotSupportedException($"IReceiverList<{typeof(T).Name} cannot be unboxed to IReceiverList<{typeof(V)}>");
				}
			}

			IEnumerator IEnumerable.GetEnumerator()
			{
				return list.GetEnumerator();
			}
			#endregion
		}

		private struct Receiver<T> where T : Component
		{
			public Action<T> Setter;
			public Predicate<T> Predicate;
		}
	}
}
