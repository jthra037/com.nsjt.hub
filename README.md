The Hub was originally intended to make it easier to access in-scene components from plain old C# objects. While the scope of it's intended uses may exceed that in the future, the intention is to keep ease of use and complexity of setup very low to help with integration in new projects.

- - - -

### Usage

For ease of use, the current access point is a single static class, `Hub`. It has a number of public methods:

- ```c#
    public static int Get<T>(Action<T> receiver) where T : Component
  ```
  - Main method for retrieving registered components. Takes an `Action<T>` receiver so it *can* be called asynchronously, in the event that there is not registrant of type `T`. The receiver is not consumed when called, so future registrations will call it again.
  Returns a handle to this receiver instance so it can be "Un-Gotten", stopping listeners regardless how `receiver` was implemented.

- ```c#
    public static int Get<T>(Action<T> receiver, Predicate<T> predicate) where T : Component
  ```
    - Same as ` Get<T>(Action<T> receiver)`, except will only call `receiver` if `predicate` returns true for a registration of `T`.

- ```c#
    public static void UnGet<T>(int hashCode) where T : Component
  ```
  - Takes a handle to a receiver instance from `Get<T>`, and removes that receiver instance. Does not remove references wherever they have been received.

- ```c#
    public static void Register<T>(T component) where T : Component
  ```
  - Registers an instance of `T` for `Get<T>` to return. If a receiver for this type, and optionally this instance meets required predicates, calling this will also call existing receivers.

- ```c#
    public static void UnRegister<T>(T component) where T : Component
  ```
  - Remove a previously registered component from the `Hub` so it can no longer get retrieved with `Get<T>`. Calling this will not remove other references to this component.

- ```c#
    public static void Clear()
  ```
  - Removes all receiver instances, and all registered components. Returns the `Hub` to a state as if it had just been instantiated.
		
- - - -
### Acknowledgements
Originally based on Yanko Oliveira's [Signals](https://github.com/yankooliveira/signals), modified to retrieve components in a typesafe way instead of send messages as well as work asynchronously with as little boxing/unboxing as possible.

Thanks to Fergui Pascual for the discussion and the code review.