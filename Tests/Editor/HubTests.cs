﻿using NUnit.Framework;
using UnityEngine;
using System;
using UnityEngine.TestTools;
using System.Collections;

namespace NSJT.ComponentHub.Editor.Tests
{
	public class HubTests
	{
		[Test]
		public void RegisterAndGet()
		{
			Hub.Clear();
			GameObject go = new GameObject();
			Hub.Register(go.transform);
			Action<Transform> assertion = t => Assert.AreSame(go.transform, t);
			Hub.Get(assertion);
		}

		[Test]
		public void PredicatedGetOneOfTwo()
		{
			Hub.Clear();
			GameObject go1 = new GameObject();
			GameObject go2 = new GameObject();
			Hub.Register(go1.transform);
			Hub.Register(go2.transform);
			Action<Transform> assertion = t => Assert.AreSame(go2.transform, t);
			Predicate<Transform> predicate = t => t.Equals(go2.transform);
			Hub.Get(assertion, predicate);
		}

		[Test]
		public void ListenForRepeatRegistrations()
		{
			Hub.Clear();
			GameObject go1 = new GameObject("First");
			Hub.Register(go1.transform);
			Transform current = null;
			Hub.Get<Transform>(t => current = t);
			Assert.AreSame(current, go1.transform, "First transform wasn't assigned to current");

			GameObject go2 = new GameObject("Second");
			Hub.Register(go2.transform);
			Assert.AreSame(current, go2.transform, "current wasn't updated to second transform registrant");
		}

		[Test]
		public void UnGetByHandle()
		{
			Hub.Clear();
			GameObject go1 = new GameObject("First");
			Hub.Register(go1.transform);
			Transform current = null;
			int handle = Hub.Get<Transform>(t => current = t);
			Assert.AreSame(current, go1.transform, "First transform wasn't assigned to current");

			Hub.UnGet<Transform>(handle);

			GameObject go2 = new GameObject("Second");
			Hub.Register(go2.transform);
			Assert.AreNotSame(current, go2.transform, "current was updated to second transform registrant");
		}

		[Test]
		public void Unregister()
		{
			Hub.Clear();
			GameObject go1 = new GameObject("First");
			GameObject go2 = new GameObject("Second");
			Hub.Register(go1.transform);
			Hub.Register(go2.transform);
			Transform result = null;

			Hub.Get<Transform>(t => result = t);
			Hub.UnRegister(result);

			Action<Transform> assertion = t => Assert.AreNotSame(result, t);
			Hub.Get(assertion);
		}

		[UnityTest]
		public IEnumerator GetBeforeRegistration()
		{
			Hub.Clear();
			GameObject go = new GameObject();
			Action<Transform> assertion = t => Assert.AreSame(go.transform, t);
			Hub.Get(assertion);

			yield return null;

			Hub.Register(go.transform);
		}
	}
}
